{
  description = "Collection of scripts for various things.";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-parts.url = "github:hercules-ci/flake-parts";
    devshell = {
      url = "github:numtide/devshell";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs @ {flake-parts, ...}:
    flake-parts.lib.mkFlake {inherit inputs;} {
      systems = ["x86_64-linux"];
      imports = [
        inputs.devshell.flakeModule
      ];

      perSystem = {pkgs, ...}: let
        create-package = {
          name,
          runtimeInputs,
        }: let
          script-text = builtins.readFile ./scripts/${name};
          script = (pkgs.writeScriptBin name script-text).overrideAttrs (old: {
            buildCommand = "${old.buildCommand}\n patchShebangs $out";
          });
        in
          {
            inherit name;
            value = pkgs.symlinkJoin {
              inherit name;
              paths = [script] ++ runtimeInputs;
              buildInputs = [pkgs.makeWrapper];
              postBuild = "wrapProgram $out/bin/${name} --prefix PATH : $out/bin";
            };
          };

        create-devshell = { name, runtimeInputs }: {
          inherit name;
          value = {
            name = "${name}-sh";
            packages = runtimeInputs ++ [pkgs.shellcheck pkgs.lazygit];
          };
        };

        scripts = with pkgs; [
          {
            name = "postgres";
            runtimeInputs = [postgresql docker-client];
          }
          {
            name = "image-move";
            runtimeInputs = [nsxiv];
          }
          {
            name = "image-remove";
            runtimeInputs = [nsxiv];
          }
          {
            name = "age2pdf";
            runtimeInputs = [qrencode typst];
          }
        ];
      in {
        packages = builtins.listToAttrs (builtins.map create-package scripts);
        devshells = builtins.listToAttrs (builtins.map create-devshell scripts);
      };
    };
}
